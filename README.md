LIS 4368 - Advanced Web Applications Development

##### Assignments:

1)[A1 README.md](a1/README.md)

  * Basic java program
  * Set up - Distributed Version Control with Git and Bitbucket
  * Set up - Java/JSP/Serviet Development Installation
  * Set up - tomcat
  * Learn git commands
  * Create two more repos, and offer links for them

2)[A2 README.md](a2/README.md)

  * Installing and using mysql
  * Creating a new user on mysql
  * Grant permissions to users
  * Creating tables, and adding information in mysql
  * Use @WebServlet (annotation) declares servlet configuration
  * Create a hello page, query page, and a query results

3)[A3 README.md](a3/README.md)

  * Create ERD based upon business rules
  * Provide screenshot of completed ERD
  * Provide DB resource links

4)[A4 README.md](a4/README.md)

  * Edit main index page
  * Create a new object to allow user input
  * Validate user input
  * Create working success page
  * Display success page with user input

5)[A5 README.md](a5/README.md)

  * Create MVC framework
  * Use server side validation
  * Get input of entries
  * Show error messages for wrong values input
  * Add entries to local host SQL
  

6)[Project 1: README.md](project1/README.md)

  * Create index page, with carousel
  * Create working links for other folders
  * Create P1 index page
  * Validate user input to each specific type
  * Give error messages when a user inputs wrong values
  * Show user when input has been properly inputed

7)[Project 2: README.md](project2/README.md)

  * MVC framework
  * Client-side server validation
  * Prepared Statements for SQL injections
  * JSTL to prevent XSS
  * Full CRUD functionality

Directories to know:
/Applications/AMPPS/mysql/bin
/Applications/tomcat/webapps
